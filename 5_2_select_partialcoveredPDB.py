from collections import defaultdict 
import os 
import random 
import argparse 

###########################################################
#### Select the BEST identity PDB candiate ################
#### 1. Raw filter with iden threshold 2-way ##############
####   iden-seq < 0.7   iden-PDB > 0.95 ###################
#### 2. Greedy Algorithm to Pick Near-Non-Overlap #########
####   Use seq segment index as reference #################
####   coverage residue >= 50 a.a. ########################
####   overlap less than 1/3 segment  #####################
###########################################################


#### Set args parameters
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--infile', type=str,
                    help = 'input raw identity csv file')
parser.add_argument('-o', '--outfile', type=str,
                    help = 'output partial PDB info csv file')
args = parser.parse_args()
in_raw_file = args.infile
out_iden_csv = args.outfile


#### Select the entry including identical PDB
def SelectCandidate(raw_identity_file,
                    out_selection_csv_file,
                    iden_threshold_seq,
                    iden_threshold_pdb):
    
    ## Raw filtering to obtain good candidate
    dict_seq_pdbs = defaultdict(list)
    dict_pair_seqseg = {}
    
    f = open(raw_identity_file, 'r')
    for line in f:
        line = line.strip()
        list_line_slices = line.split(',')
        iden_against_seq = list_line_slices[2].strip()
        iden_against_pdb = list_line_slices[3].strip()

        seq1_whole_header = list_line_slices[0].strip()
        pdb_header = list_line_slices[1].strip()
        
        seg_seq = list_line_slices[-2].strip()
        coverage_seqAA = int(seg_seq.split('_')[-1]) -\
                           int(seg_seq.split('_')[-2]) 
        
        pdb_seg = list_line_slices[-1].strip()
        coverage_pdbAA = int(pdb_seg.split('_')[-1]) -\
                         int(pdb_seg.split('_')[-2]) 
        
        iden_against_seq = float(iden_against_seq)
        iden_against_pdb = float(iden_against_pdb)

        if iden_against_seq < iden_threshold_seq:
            if iden_against_pdb >= iden_threshold_pdb:
                if coverage_pdbAA >= 50:
                    ## add the candidate to hash
                    dict_seq_pdbs [seq1_whole_header].append(pdb_header)
                    
                    pair_name = seq1_whole_header + ',' + pdb_header 
                    dict_pair_seqseg [pair_name] = seg_seq
    f.close()


    #### Remove Overlapping PDB 
    #### Greedy Algorithm applied
    #### overlap > 1/3 ., adopt longest 

    dict_seq_selectPDBs = {}

    for each_seq, list_pdbs in dict_seq_pdbs.items():
        count_pdb = len(list_pdbs)
        
        ## list that store pdb need to clear/Remove OUT
        list_clear_pdb = []

        ## randomly shuffle the order of PDB in list, equivent to randomly choose one seed
        random.shuffle(list_pdbs)

        if count_pdb > 1:
            for i in range(0, count_pdb-1, 1):
                for j in range(i+1, count_pdb, 1):

                    pair_name_i = each_seq + ',' + list_pdbs[i]
                    seq_seg_cov_i = dict_pair_seqseg [pair_name_i]

                    pair_name_j = each_seq + ',' + list_pdbs[j]
                    seq_seg_cov_j = dict_pair_seqseg [pair_name_j]

                    element_i = list_pdbs[i]
                    element_j = list_pdbs[j]
                    
                    ## Compare the seg coverage, 
                    ## if overlap > 1/3, remove lower coverage One
                    ## in total C(3,2)==6 types overlapping
                    min_i = int(seq_seg_cov_i.split('_')[0])
                    max_i = int(seq_seg_cov_i.split('_')[-1])
                    length_i = max_i - min_i + 1

                    min_j = int(seq_seg_cov_j.split('_')[0])
                    max_j = int(seq_seg_cov_j.split('_')[-1])
                    length_j = max_j - min_j + 1

                    ## Deal with 4 Conditions, i as reference
                    # min_j in the left
                    if min_j < min_i:
                        if max_j < min_i:
                            pass
                        elif max_j > max_i:
                            list_clear_pdb.append(element_i)
                        elif max_j >= (min_i + length_i/3):
                            if length_i >= length_j:
                                list_clear_pdb.append(element_j)
                            else:
                                list_clear_pdb.append(element_i)
                        else:
                            pass
                    # min_j in the right
                    elif min_j >= max_i:
                        pass
                    # min_j equal min_i
                    elif min_j == min_i:
                        if max_j <= max_i:
                            list_clear_pdb.append(element_j)
                        else:
                            list_clear_pdb.append(element_i)
                    # min_j in middle(min_i, max_i)
                    elif min_i < min_j < max_i:
                        if min_j < (min_i + length_i/3):
                            if length_j >= length_i:
                                list_clear_pdb.append(element_i)
                            else:
                                list_clear_pdb.append(element_j)
                        else:
                            pass
        ## count ==1. No action
        else:
            pass

        list_selected_pdb = [] 
        # the unique delete pdb
        list_unique_clear_pdb = []
        for e in list_clear_pdb:
            if e not in list_unique_clear_pdb:
                list_unique_clear_pdb.append(e)
        # obtain select, original MINUS del
        for e in list_pdbs:
            if e not in list_unique_clear_pdb:
                list_selected_pdb.append(e)

        dict_seq_selectPDBs [each_seq] = list_selected_pdb
    ## DONE
    
    ## OutPut 
    out_F = open(out_iden_csv, 'w')
    outF_header = 'queryID,pdbchainID,identityQuery,identityPDB,totalLength,coverageQuery,coveragePDB' + "\n"
    out_F.write(outF_header)
    
    for k, v in dict_seq_selectPDBs.items():
        for each_pdb in v:
            prefix_header = k + ',' + each_pdb

            f = open(raw_identity_file, 'r')
            for line in f:
                if line.startswith(prefix_header):
                    out_F.write(line)
            f.close()
    out_F.close()
    
    return 0

SelectCandidate(raw_identity_file = in_raw_file,
                out_selection_csv_file = out_iden_csv ,
                iden_threshold_seq = 0.7,
                iden_threshold_pdb = 0.95)
